//*****************************************************************************
//
// pinout.c - Function to configure the device pins on the DK-TM4C129X.
// 
// This is part of revision 2.1.4.178 of the DK-TM4C129X Firmware Package.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "pinout.h"

void
PinoutSet(void)
{
    //
    // Enable the GPIO port that is used for the on-board LED.
    //
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);

    //
    // Check if the peripheral access is enabled.
    //
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOQ)){
    }
    while(!ROM_SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOQ)){
    }
        //
    // PQ7 and N5 is used for LED blue and LED red
    //
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTQ_BASE, GPIO_PIN_7);
    ROM_GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_5);  
}