/** @file main.c (structural command)
*   brief startup code for FreeRTOS TIVA 129
*              
*   This file contains the initializaction of the scheduler.
*
*   @author Julio Cesar Gachuzo Elias...
*
*   @bug No known bugs.
*
*   Using FreeRTOS v10.2.0
*
*   freertos_demo.c - Simple FreeRTOS example.
*
*   Copyright (c) 2009-2017 Texas Instruments Incorporated.  All rights reserved.
*   Software License Agreement
*
*   Texas Instruments (TI) is supplying this software for use solely and
*   exclusively on TI's microcontroller products. The software is owned by
*   TI and/or its suppliers, and is protected under applicable copyright
*   laws. You may not combine this software with "viral" open-source
*   software in order to form a larger program.
* 
*   THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
*   NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
*   NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*   A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
*   CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
*   DAMAGES, FOR ANY REASON WHATSOEVER.
* 
*   This is part of revision 2.1.4.178 of the DK-TM4C129X Firmware Package.
*/

/*General C header files*/
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom_map.h"
#include "pinout.h"

/*! FreeRTOS specific includes*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#define mainDELAY_LOOP_COUNT     (0x1ffffE)

volatile uint32_t g_ui32SysClock = 0U;

/** @brief Prototype Tasks.*/
void vTask1( void *pvParameters );
void vTask2( void *pvParameters );
  

void vTask1( void *pvParameters ){
  
  for( ;; ){
    uint32_t i;
    
    // Turn on the blue LED.
    GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_7, GPIO_PIN_7);
    for(i = 0; i < mainDELAY_LOOP_COUNT; i++);
    
  //
  // Turn off the blue LED.
  //
  GPIOPinWrite(GPIO_PORTQ_BASE, GPIO_PIN_7, 0x0);
    for(i = 0; i < mainDELAY_LOOP_COUNT; i++);
  }
}

  void vTask2( void *pvParameters ){
  for( ;; ){
    uint32_t j;
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_5, 0X0);
    for(j = 0; j < mainDELAY_LOOP_COUNT; j++);
    
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_5, GPIO_PIN_5);
    for(j= 0; j < mainDELAY_LOOP_COUNT; j++);
  }
}



int main(){

  g_ui32SysClock =  MAP_SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                          SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                                          SYSCTL_CFG_VCO_480), 120000000);
  //
  // Initialize the device pinout appropriately for this board.
  //
  PinoutSet();

/****************************Creating the Tasks********************************/

/* Create one of the two tasks. Note that a real application should check
the return value of the xTaskCreate() call to ensure the task was created
successfully. */
xTaskCreate( *vTask1,/*Task service function*/
            "Task 1",/* descriptive name for the task ASCII */
            100,     /* stack size */
            NULL,    /* pointer of use context */
            1,       /* priority  */
            NULL );  /* task handler, manipulates the tasks actual conditions */
/* Create the other task in exactly the same way and at the same priority. */
if(xTaskCreate( vTask2, "Task 2", 100, NULL, 1, NULL) != pdTRUE){
  return(1);
}
/* Start the scheduler so the tasks start executing. */
vTaskStartScheduler();
  
  while(1){
  
  
  }
  
  //return 0;
}


//*****************************************************************************
//
// This hook is called by FreeRTOS when an stack overflow error is detected.
//
//*****************************************************************************
/** @brief Stack Overflow Hook.
 *  
 *  This hook is called by FreeRTOS when an stack overflow error is detected.
 */
void
vApplicationStackOverflowHook(xTaskHandle *pxTask, char *pcTaskName){

    //
    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.
    //
    while(1)
    {
    }
}
//*****************************************************************************
