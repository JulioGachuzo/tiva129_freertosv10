//*****************************************************************************
//
// pinout.h - Prototype for the function to configure the device pins on the
//            DK-TM4C129X.
// 
// This is part of revision 2.1.4.178 of the DK-TM4C129X Firmware Package.
//
//*****************************************************************************

#ifndef __DRIVERS_PINOUT_H__
#define __DRIVERS_PINOUT_H__

//*****************************************************************************
// Prototypes.
//*****************************************************************************
extern void PinoutSet(void);



#endif // __DRIVERS_PINOUT_H__